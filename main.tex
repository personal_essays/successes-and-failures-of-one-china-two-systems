% Font size
\documentclass[12pt,a4paper,british]{article}

% utf8 support
\usepackage[utf8]{inputenc}

\usepackage{hyperref}

\usepackage{babel,xpatch}

\usepackage[autostyle]{csquotes}

% Referencing
\usepackage[backend=biber,style=apa]{biblatex}
\addbibresource{references.bib}
% break bilbatex reference URL on number
\setcounter{biburlnumpenalty}{9000}

% packages for Header and Footer
\usepackage{lastpage}
\usepackage{fancyhdr} 

% quotations
\MakeOuterQuote{"}

%!TeX spellcheck = en-GB

%----------------------------------------------------------------------------------------
%	TITLE SECTION
%----------------------------------------------------------------------------------------

\title{\textbf{Evaluate the successes and failures of China's \textit{One China, Two Systems} policy in Hong Kong.}}

\author{\textit{2270405}}

 % Date, use \date{} for no date
\date{\today}

% save these variables for later use under different name
\makeatletter
\let\doctitle\@title
\let\docauthor\@author
\let\docdate\@date
\makeatother

%----------------------------------------------------------------------------------------
%	HEADER AND FOOTER SECTION
%----------------------------------------------------------------------------------------
\pagestyle{fancy}

% sets both header and footer to nothing
\fancyhf{}

% Header
% remove horizontal header bar
\renewcommand{\headrulewidth}{0pt}
% left hand side header
\lhead{Chinese Politics - Literature review}
% right hand side header
\rhead{\docauthor}
\setlength{\headheight}{15pt}

% Footer
% center of footer
\cfoot{\thepage\ of \pageref{LastPage}}

%------------------------------------------------------------------------

\begin{document}

\maketitle % Print the title section

%------------------------------------------------------------------------
%	ABSTRACT AND KEYWORDS
%------------------------------------------------------------------------

% Uncomment to change the name of the abstract to something else
\renewcommand{\abstractname}{Literature review}

\begin{abstract}
    \fullcite{chenAutonomyHongKong2018}
    \paragraph{}
    \fullcite{scottOneCountryTwo2017}
    \paragraph{}
    \fullcite{soOneCountryTwo2011}
    \paragraph{}
    \fullcite{chanThirtyYearsTiananmen2019}
\end{abstract}

% Keywords
\hspace*{3.6mm}\textit{Keywords:} Hong Kong, China, One China Two Systems

% Vertical whitespace between the abstract and first section
\vspace{30pt}


%------------------------------------------------------------------------
%	ESSAY BODY
%------------------------------------------------------------------------

\section*{Introduction}


\paragraph{}

On the 23\textsuperscript{rd} of June 2013 the United States' (US) Justice Department filed three felony charges against "Edward J. Snowden" accusing him of \textit{"Unauthorized Communication of National Defence Information"} to the journalists he had convened in Hong Kong (HK) \autocite{usjusticedepartmentCriminalComplaintEdward2013}. The American authorities failed to provide his passport number to the HK immigration authorities and mistakenly referred to him as \textit{"Edward James Snowden"} instead of \textit{"Edward Joseph Snowden"} which was claimed to be the reason why HK authorities allowed his departure to Moscow \autocite{kielsgardHongKongFailure2014}.

This particular incident (in the midst of wider events) has led to speculations as to what would have happened had Snowden remained in HK and faced the extradition request. \textcite{kielsgardHongKongFailure2014} attempt this thought exercise and note on the conflict between the autonomy of HK's judicial system and Beijing's authority over its foreign and defence affairs. Although as the article states, it has traditionally not done so for extradition matters, the Central People's Government (CPG) could have overriden a HK ruling on Snowden's extradition.

This review will critic some of the literature that evaluates this opposition between Chinese Sovereignty and HK's guarantees of autonomy under the OCTS policy. We find that works like \textcite{chenAutonomyHongKong2018}'s and \textcite{chanThirtyYearsTiananmen2019}'s consideration of the OCTS policy as a compromise between a federalist and unitary state correctly underlines the reasons for this opposition. \textcite{soOneCountryTwo2011}'s "crisis-transformation" perspective overlooks some aspects of HK's weak autonomy, but do however provide useful insight into the shifting balance of power towards Beijing that has allowed it to impose its conception of the OCTS model onto HK. In fact, \textcite{scottOneCountryTwo2017} and \textcite{chenAutonomyHongKong2018} give an insight into this Chinese perspective of the OCTS model, uncovering the policy's innate and fundamental contradictions from which we conclude that perhaps the OCTS model was doomed to fail because it confronts authoritarianism with political liberalism yet fails to recouncil them.


\section*{}

\subsection*{A weak autonomy}

\paragraph{}
The notion of autonomy is key, \textcite{chenAutonomyHongKong2018} refers to it as a continuous notion rather than a discreet one: a state has a certain \textit{degree} of autonomy depending on the the aspects of governance it has been delegated by the authority that would otherwise hold them. This is illustrated with the concept of a federation of states bound by a constitution that divides power amongst them and the federal government. Disputes over this division are settled by an \textit{independent} Constitutional Court that interprets the Constitution.

The Basic Law of Hong Kong has been compared to a \textit{mini constitution} \autocites[85]{scottOneCountryTwo2017}[104]{soOneCountryTwo2011}, to which China is \textit{"constitutionally mandated"} \autocite[442]{chanThirtyYearsTiananmen2019}. However, the constitutional characteristics of the Basic Law are weak: article 158 of the Basic Law confers the National People's Congress' Standing Committee (NPCSC) the right to interpret the Basic Law \autocite[40]{chenAutonomyHongKong2018} conflicting with the HK legislature's same right \autocite[442]{chanThirtyYearsTiananmen2019}. Furthermore, article 17 states the NPCSC's right to nullify a HK law that exceeds its autonomy. While the latter right has never been exercised and the former only on five occasions, their existence challenge the autonomy of HK by making the CPG subject of the law and the NPCSC its arbiter, whereas in practice the two institutions are questionably independent of one another \autocite[39-40]{chenAutonomyHongKong2018}.

\textcite{chenAutonomyHongKong2018} notes that the People's Republic of China (PRC) cannot accept a federal structure, a point highlighted by his distinction between \textit{unitary} and \textit{federal} states. This is key in understanding the Chinese position on the argument which tends to accentuate "One Country" opposing the Hongkongers' emphasis on "Two systems". Although not as extensively as \textcite{chenAutonomyHongKong2018}, all of the articles touch on this aspect, except \textcite{soOneCountryTwo2011} who approaches the issue from a "non legalistic" perspective to conclude that \textit{"the framework has provided a firm institutional foundation for the unification process"} \autocite[114]{soOneCountryTwo2011}. The political context of \textcite{soOneCountryTwo2011}'s article has evolved, with 2003 protesters counted in the hundreds of thousands \autocite{leePerceptualBasesCollective2010} while the order of magnitude in 2019 is closer to the millions \autocite{scmpreportersItHappenedHong2019}. Nonetheless, \textcite{soOneCountryTwo2011}'s decisive conclusion on the success of the the OCTS policy neglects the Basic Law's weak protections of HK's autonomy. His non legalistic perspective does however highlight a successful economic integration between HK and the Mainland.

\subsection*{Economic integration}

\paragraph{}
The perspective of an end to the liberal economic order in the years leading up to the 1997 expiration of the British lease on HK's "New Territories" caused financial market fluctuations, the relocation of capital outside of HK, a sharp decrease of the HK dollar's value and an economic downturn \autocite{soOneCountryTwo2011}. Such a recession was in neither London's or Beijing's interest, that is why the economic autonomy of HK, notably in terms of taxation, customs, free flow of capital, private property and the city's "capitalist system" were all formulated in the Joint Declaration \autocite[143-146]{peoplesrepublicofchinaJointDeclarationGovernment1984}.

These guarantees of the \textit{laissez faire} economic paradigm in HK allowed it to complete its transition to a services economy by exporting its industrial activity to the Mainland, notably through the \textit{Closer Economic Participation Agreement} (CEPA) in 2003 \autocite[105,110]{soOneCountryTwo2011}. The city became an international gateway to the Chinese workforce and companies, hereby strongly integrating the two economies, notably with HK's key asset: its financial sector. It provided for the Mainland both its stock market, an important destination for international Initial Public Offerings (IPO) and a sizeable banking industry with lenders, such as HSBC, that were a significant source of investment into China \autocite[194-195]{yeungOneCountryTwo2015}.

However, eight years after \textcite{soOneCountryTwo2011}'s article, this dependence of the Mainland on HK has shifted: \textit{"The economic liberalization that to some extent enabled China to survive the global liberal shockwaves in 1989 will likewise allow it to manage the pushbacks from its most liberal region in 2019"} \autocite[450]{chanThirtyYearsTiananmen2019}. The Mainland's growth enables it to challenge HK's contribution to its economy, a paradigm shift of economic power to the Mainland that \textcite{scottOneCountryTwo2017} and \textcite{chenAutonomyHongKong2018} fail to mention in their strictly political evaluation of the OCTS policy.

Furthermore, the rise of challengers to HK in the Pearl River Delta and elsewhere in China directly results from the city's economic integration. HK is now rivalled by the surrounding Metropoleis of Guangzhou and more importantly today: Shenzhen as well as the more remote Shanghai, with the latter two respectively competing with HK's services and financial sectors \autocite{chanThirtyYearsTiananmen2019,chenLostCompetitionRethinking2018}.

Despite some economic downturns, it is not the growth rate of HK that has declined, rather, it is the share of its contribution to the Chinese Gross Domestic Product (GDP), down from 13\% in 1992 to 2\% in 2012. This is a good illustrator of the new power paradigm in the Mainland's relationship to HK \autocite[193-194]{yeungOneCountryTwo2015}. It is in part this shift that has conferred Beijing more confidence to assert its "One China" agenda of sovereignty over HK \autocite[113]{soOneCountryTwo2011}.

\subsection*{Democracy with Chinese characteristics}

\paragraph{}
This first point of the Joint Declaration affirms the resumption of the PRC's sovereignty over HK on the 1\textsuperscript{st} of July 1997 \autocite[142]{peoplesrepublicofchinaJointDeclarationGovernment1984}. Works like \textcite{chanThirtyYearsTiananmen2019}'s or \textcite{scottOneCountryTwo2017}'s reason with "Western conceptions" of self-determination, the American Federal system or the British conceptions of devolution are not applicable to the case of HK and China according to \textcite{chenAutonomyHongKong2018}. As has been discussed, the PRC cannot accept federalism, this is because of its single party nature and tradition of the Chinese Communist Party's (CCP) \textit{"united front"} \autocite[44]{chenAutonomyHongKong2018}. \textcite{chenAutonomyHongKong2018} notes that the Chinese conception of HK's autonomy is the right of Hongkongers to be governed by Hongkongers as appointed by Beijing, notably with the CPG's nomination power over the Chief Executive not being a mere formality \autocite{chenAutonomyHongKong2018}. As Xiao Weiyun, one of the drafters of the Basic Law, claims: it is not possible to understand \textit{One Country, Two Systems} without understanding the Chinese conception of \textit{One Country} \autocite{scottOneCountryTwo2017}.

In this sense, the CPG has successfully imposed its conception of autonomy onto HK under the OCTS policy while failing to make it accepted. Insofar as the OCTS policy in HK was to serve as a model of integration for Taiwan, the CPG has failed precisely because Honkongers have opposed it \autocite[451]{chanThirtyYearsTiananmen2019}. This point is only brought up by \textcite{chanThirtyYearsTiananmen2019}: despite mentioning the policy's original design for Taiwanese reintegration, the rest of the reviewed works have failed to note the Taiwanese opposition to the OCTS model.

This constant opposition between the two aspects of the OCTS model have allowed some to paint the policy as fundamentally flawed \autocite{chanThirtyYearsTiananmen2019,scottOneCountryTwo2017}, while others believe a successful balance of different interests can be met for a successful reunification \autocite{chenAutonomyHongKong2018,soOneCountryTwo2011}.

\subsection*{A journey without a destination}

\begin{quote}
    \textit{
        "The ultimate aim is the selection of the Chief Executive by
        universal suffrage upon nomination by a broadly representative
        nominating committee in accordance with democratic procedures."
    }
    % quote source
    \begin{flushright} Article 45 of the Basic Law \\
        \textcite{nationalpeoplescongressBasicLawHong1990}\end{flushright}
\end{quote}

\textit{"The ultimate aim"} is a statement strongly lacking in meaning that echoes its equally vague origin in the Sino-British Joint Declaration: \textit{"The chief executive will be appointed by the Central People's Government on the basis of the result of elections or consultations [...]"} \autocite[143]{peoplesrepublicofchinaJointDeclarationGovernment1984}. \textcite{soOneCountryTwo2011} notes the OCTS policy is a constant struggle to restore balance between "One Country" and "Two Systems", but so long as \textit{"the 'One Country, Two Systems' policy is accepted by all the actors of national unification, their differences can be worked out"} \autocite[114]{soOneCountryTwo2011}.

This perspective is challenged by the more contemporary literature, for which this balance is paradoxical as \textit{"there might be a connection between the limited democracy within the SAR's political system and the huge scope of its autonomy"} \autocite[41]{chenAutonomyHongKong2018}. This notion of the OCTS policy being an irreconcilable paradox is echoed in \textcite[442]{chanThirtyYearsTiananmen2019} which stresses that the policy is \textit{"novel in ambition"} but \textit{"the Basic Law incorporates the ideology of both sides without fully specifying how their differences can be resolved but preserved"}. \textcite{scottOneCountryTwo2017} concludes that, considering the erosion of HK's autonomy, the OCTS policy's \textit{"days as a legitimating ideology look to be numbered unless action is soon taken on both sides of the border to show that the concept means something more than empty promises"}

From this perspective of "empty promises", there might as well be no policy, because it offers no solution to reconcile the diverging interests upon which it tries to ambiguously force coexistence. \textit{\textquote{To some extent the problem is inherent in the system. 'One country, two systems' is a journey without destination}} \autocite[116]{liuFirstTungCheehwa2002}, to some extent, the policy is its own failure.

\section*{Conclusion}

\paragraph{}
\textcite{scottOneCountryTwo2017}, \textcite{chanThirtyYearsTiananmen2019} and \textcite{chenAutonomyHongKong2018} all provide an account of Beijing and HK's diverging interpretations of the OCTS policy. \textcite{chenAutonomyHongKong2018}'s differentiation of unitary and federal states and the OCTS policy's situation in between provides the most insight into the ambiguous legal guarantees of HK's autonomy. \textcite{scottOneCountryTwo2017}'s \textit{legitimating ideology} perspective on the OCTS policy aptly illustrates the delicate balance upon which rests the fragile order built by the Joint Declaration. Thus, all three of these articles justly conclude on the fundamental flaws of the OCTS policy, noting that its success only holds as long as the balance of power between HK and China stands.

The strength of \textcite{soOneCountryTwo2011}'s work lies in its "non-legalistic" approach: while it overlooks the policy's foundational problems as well as Beijing's "One Country perspective", it does underline with depth the shift in the economic balances of power between HK and the Mainland which is a major cause of today's unbalance in the OCTS model. \textcite{soOneCountryTwo2011}'s work is the only that concludes with a positive outlook on the OCTS's ability to successfully reintegrate HK into China. This is probably due to the fact it is older by five to ten years compared to the other works reviewed, with significant developments having taken place in the interim.

It is precisely this shift of "power" in the economic relations between HK and the Mainland that have allowed \textcite{chenAutonomyHongKong2018} and \textcite{scottOneCountryTwo2017} to define the diverging notion of democracy "with Chinese characteristics" that Beijing has progressively imposed upon HK in recent years due to its regained leadership in the HK-China relationship. \textcite{chanThirtyYearsTiananmen2019} fails to consider this Chinese perspective on the democratization of HK, it does however note on the underlying paradox of the OCTS policy.

In fact most of the works, except for \textcite{soOneCountryTwo2011} correctly point to the fundamental contradictions of the OCTS policy. They show that to some extent, the political liberalism of HK is incompatible with the authoritarianism of the Mainland, despite economic the upholding of the liberal economic paradigm in HK.

On a last note, \textcite{chenAutonomyHongKong2018}'s and to a lesser extent, \textcite{scottOneCountryTwo2017}'s works have shed some insight into some Chinese (Mainland) perspectives of the OCTS policy. These perspectives are not developed with enough depth in this corpus. A review of Mainland Chinese literature on the policy's implementation might reveal an alternative conclusion to this review's emphasis on the fundamental contradictions of the OCTS policy.


\newpage
%----------------------------------------------------------------------------------------
%	BIBLIOGRAPHY
%----------------------------------------------------------------------------------------

\emergencystretch=1em

\printbibliography[title=References]

%----------------------------------------------------------------------------------------

% \paragraph{}

% % TODO: redo this, we are not asking the same questions anymore
% What is the \textit{One China, Two Systems} (OCTS) policy (also Known as \textit{One China, Two Systems} agreed on for Hong Kong (HK) in the Sino-British Joint Declaration \autocite{peoplesrepublicofchinaJointDeclarationGovernment1984}? By answering this question, we can define the policy's aims upon which we can judge its "successes" and "failures". Is it a strategy for re-integrating HK with the Mainland after the 1997 reunification? Is it designed to be an exemplary model of integration to encourage Taiwanese reunification? Perhaps it is a temporal extension to the liberal economic paradigm in HK? By reviewing the key works cited above and taking %need to find better word
% from other literature in the field, we will argue on the successes % can you say argue on the successes?
% and failures of these different objectives. We will conclude that the Policy as a whole cannot be deemed a success because it is at its core a compromise of interests that have re-emerged to oppose one another leaving the OCTS policy with no purpose but its own.


% \paragraph{}

% \textcite{scottOneCountryTwo2017}'s argument OCTS is a legitimating ideology for HK Government. While CN government has started to reintegrate HK into China through OCTS, it has done so too heavy handedly. This has led to opposition from Hongkongers which have called for a review of the OCTS principle. The article present the views of the "many Honkongers" that view the OCTS as a contract with tripode consent (Britain, China, Honk Kong). It shows how the HK Government's high degree of autonomy has consistently been undermined by Beijing who forced its reforms through (High Speed railway). It discusses the punishing of the Judicial system when it ruled unfavourably to Beijing's interests (Ng Ka Ling v Director of Immigration, "judicial retreat" after ruling). The article undelines what is strongly apparented to Beijing attempts at silencing the media through abduction of bookshop owners and purchashing of controlling shares in Major HK media (directly or inderectly through companies). Lastly it demonstrates that the Central Committee's wishes for a delay in implementation of universal sufferage have been fullfilled by the HK government and the judicial system who have consistently delayed its implementation.

% Good seperation of concerns, economic, cultural, law etc...

% \paragraph{}

% \textcite{yeungOneCountryTwo2015} claims OCTS has made the success of HK, notably by allowing its Financial sector legislation, inherited from British Colonial legacy, to grow apart from the Chinese legsilation. This has notably allowed HK to become a prime destination for Chinese IPOs, a major source of funding for mainland China while also attracting Foreign Investment in China. The article mentions opposition to HK government's refusal to implement consititional universal sufferage, but mainly as a source of disruption for the financial market. It ends on a hopeful note that the OCTS has workd well for HK and will continue to do so, with the priority being to preserve it through dialog on reform.

% \paragraph{}

% \textcite{soOneCountryTwo2011} analyzes the OCTS policy through what is calls a "crisis transformation framework" exploring the policy, its consequences and evolutions through multiple key "crisis" events. The OCTS was conceived as form of \textit{guarantee of liberties} by the PRC to ease Hong Kong and its financial markets into accepting Chinese claims on the British colony. After the handover in 1997, economic integration with the mainland provided Hong Kong cheap labour for its transition to the services sector which allowed it to become the international gateway to the Chinese economy.


% legitimating Ideology

% OCTS policy is legitimating ideology for the HK people's acceptance of Chinese sovereignty.

% The erosion of its principles notably through "crises" translates into HK opposition to the HK government and the CPG.

% So and Scott agree on that point

% - Integration after reunification of Hong Kong with the mainland?
% - An exemplary model of transition to encourage Taiwanese reunification?
% - An extension of the economically liberal paradigm in Hong Kong?
% - An way of introducing Western liberal democracy into Mainland China?
% - Did it in fact not have a goal and it is its own end? The OCTS' goal is to implement the OCTS? a Journey without goal


% The policy emerged as a strategy for the reunification of Taiwan \autocite{chanThirtyYearsTiananmen2019,chenAutonomyHongKong2018,scottOneCountryTwo2017} but was formulated in the Sino-British joint declaration of 1984, a binding international agreement registered at the United Nations regarding the future of HK after the 1997 handover \autocite{peoplesrepublicofchinaJointDeclarationGovernment1984}.


% \begin{itemize}
%     \item The document recognises the sovereignty of the People's Republic of China (PRC) the HK area after its restitution by the UK.
%     \item It a defines a \textit{Special Administrative Region} (SAR) that will enjoy a "high degree of autonomy" except in foreign and defence affairs.
%     \item This SAR will be endowed with "executive, legislative and independent judicial power" governed by "local inhabitants".
%     \item The Chief Executive will be nominated by "elections or consultation", be appointed by the CPG and it will nominate principal officials
% \end{itemize}

% That is up until the 2019 Hong Kong extradition bill \autocite{leeFugitiveOffendersMutual2019}

% HK law on extradition, notably its exception for political offenses, and the Central People's Government (CPG) right to intervene on the basis of China's defence or Foreign affairs interests.

% This "would be" conflict of interests embodies the ambivalent nature of the \textit{One China, Two Systems} (OCTS) policy, a careful equilibrium between HK's autonomy and China's sovereignty over the city.

% TODO smoother link with main argument about OCTS, maybe with the question of sovereignty?


% conclusion will probably remain the same but the questions asked which form the plan will probably need to change e.g. do I really want to touch on the subject of Taiwan?

\end{document}
